import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';

@Entity()
export class Book {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    title: string;

    @Column()
    author: string;

    @Column()
    publisher: string;

    @Column()
    description: string;

    @Column()
    source: string;

    @Column()
    count: number;

    @Column()
    pages: number;

    @Column()
    price: number;

    @Column()
    published: Date;
}