import { Repository, EntityRepository } from "typeorm";
import { Book } from "./book.entity";
import { GetBooksListViewModel, GetBooksListViewModelItem } from "./view-models/get-books-list.viewmodel";

@EntityRepository(Book)
export class BookRepository extends Repository<Book>{
    
    async GetBooks():Promise<GetBooksListViewModel>{

        let model = new GetBooksListViewModel();
        model.Books = new Array<GetBooksListViewModelItem>();
        const booksList = await this.find();

        if(booksList === null || booksList.length === 0){
            return model;
        }
        
        booksList.forEach((book)=>{
            let item = new GetBooksListViewModelItem();
            item.author = book.author;
            item.count = book.count;
            item.description = book.description;
            item.pages = book.pages;
            item.price = book.price;
            item.published = book.published;
            item.publisher = book.publisher;
            item.source = book.source;
            item.title = book.title;
            item.id = book.id.toString();

            model.Books.push(item);           
        });

        return model;
      }
}