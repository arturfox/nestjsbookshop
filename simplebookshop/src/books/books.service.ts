import { Injectable } from '@nestjs/common';
import { Book } from './book.entity';
import { BookRepository } from './book.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { GetBooksListViewModel } from './view-models/get-books-list.viewmodel';
import { AddBookModel } from './view-models/models/add-book.model';
import { ResultModel } from '../shared/result-model';
import { GetBookShortInfoViewModel } from './view-models/get-book-short-info.view-model';
import { ObjectID } from 'mongodb';
import { EditBookModel } from './view-models/models/edit-book-model';


@Injectable()
export class BooksService {
    constructor(
        @InjectRepository(Book)
        private readonly bookRepository: BookRepository) {
    }

    GetBooks(): Promise<GetBooksListViewModel> {
        return this.bookRepository.GetBooks();
    }

    async CheckIsBookExist(bookId: string): Promise<boolean> {

        const book = await this.bookRepository.findOne(bookId);
        return !(book == undefined);
    }

    async AddBook(bookModel: AddBookModel): Promise<ResultModel> {

        let result = new ResultModel();

        let book = new Book();
        book.author = bookModel.author;
        book.count = bookModel.count;
        book.description = bookModel.description;
        book.pages = bookModel.pages;
        book.price = bookModel.price;
        book.published = bookModel.published;
        book.publisher = bookModel.publisher;
        book.source = bookModel.source;
        book.title = bookModel.title;


        const insertResult = await this.bookRepository.insert(book);
        result.isSuceed = !(insertResult.generatedMaps == null
            || insertResult.generatedMaps == undefined);

        result.message = result.isSuceed ? "book successfully added to catalog" : "something went wrong while saving book to the catalog";

        return result;
    }

    async GetBooksShortDataByIds(ids: ObjectID[]): Promise<GetBookShortInfoViewModel[]> {

        let data = new Array<GetBookShortInfoViewModel>();
        const books = await this.bookRepository.findByIds(ids);

        books.forEach((book) => {
            let bookModel = new GetBookShortInfoViewModel();
            bookModel.bookId = book.id.toString();
            bookModel.price = book.price;
            bookModel.title = book.title;
            bookModel.source = book.source;

            data.push(bookModel);
        });

        return data;
    }

    async DeleteBook(bookId: string): Promise<void> {

            await this.bookRepository.delete(bookId);       
    }

    async EditBook(editBookModel: EditBookModel): Promise<void> {

        await this.bookRepository.update(editBookModel.itemId, {
            title: editBookModel.title,
            description: editBookModel.description,
            count: editBookModel.count,
            price: editBookModel.price
        });
    }
}
