export class GetBooksListViewModel {

    public Books: GetBooksListViewModelItem[];

    constructor() {
    }
}

export class GetBooksListViewModelItem {

    id: string;

    title: string;

    author: string;

    publisher: string;

    description: string;

    source: string;

    count: Number;

    pages: Number;

    price: Number;

    published: Date;
}