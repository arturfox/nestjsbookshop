export class EditBookModel {

    itemId: string;

    title: string;

    description: string;

    count: number;

    price: number;
}