export class AddBookModel {

    title: string;

    author: string;

    publisher: string;

    description: string;

    source: string;

    count: number;

    pages: number;

    price: number;

    published: Date;
}
