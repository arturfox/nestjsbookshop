export class GetBookShortInfoViewModel {

    bookId: string;

    title: string;  

    source: string;

    price: number;
}