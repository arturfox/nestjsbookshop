import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BooksService } from './books.service';
import { SharedModule } from '../shared/shared.module';
import { BookRepository } from './book.repository';

@Module({
  imports: [SharedModule,
    TypeOrmModule.forFeature([BookRepository])],
  exports: [BooksService, TypeOrmModule],
  providers: [BooksService]
})
export class BooksModule {}