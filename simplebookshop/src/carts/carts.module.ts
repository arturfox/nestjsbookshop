import { Module } from '@nestjs/common';
import { CartsService } from './carts.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from '../shared/shared.module';
import { CartRepository } from './cart.repository';

@Module({
  imports: [SharedModule,
    TypeOrmModule.forFeature([CartRepository])],
  exports: [CartsService, TypeOrmModule],
  providers: [CartsService]
})
export class CartsModule { }