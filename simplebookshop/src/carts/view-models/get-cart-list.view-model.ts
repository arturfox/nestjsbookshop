export class GetCartListViewModel {

    cartId: string;

    positions: GetCartlistViewModelItem[];
}

export class GetCartlistViewModelItem {

    itemId: string;

    count: number;
}