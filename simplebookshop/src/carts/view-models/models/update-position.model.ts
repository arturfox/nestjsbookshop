import { CartPositionAction } from "../../cart-position-action.enum";

export class UpdatePositionModel {

    itemId: string;
    action: CartPositionAction
}
