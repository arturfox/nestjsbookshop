import { Injectable } from '@nestjs/common';
import { Cart, CartPosition } from './cart.entity';
import { CartRepository } from './cart.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { GetCartListViewModel, GetCartlistViewModelItem } from './view-models/get-cart-list.view-model';
import { ResultModel } from '../shared/result-model';
import { AddPositionModel } from './view-models/models/add-position.model';
import { UpdatePositionModel } from './view-models/models/update-position.model';
import { CartPositionAction } from './cart-position-action.enum';
import { RemoveBookFromCartModel } from './view-models/models/remove-book-from-cart.model';

@Injectable()
export class CartsService {

    constructor(
        @InjectRepository(Cart)
        private readonly cartRepository: CartRepository) {
    }

    async GetCartListByUserId(userId: string): Promise<GetCartListViewModel> {

        let result = new GetCartListViewModel();

        let carts = (await (this.cartRepository.find())).filter(cart => cart.userId == userId);

        if (carts === null || carts.length === 0) {

            let cart = new Cart();
            cart.userId = userId;
            cart.positions = new Array<CartPosition>();
            this.cartRepository.insert(cart);

            carts = (await (this.cartRepository.find())).filter(cart => cart.userId == userId);
            result.cartId =  carts[0].id.toString();
            result.positions = carts[0].positions;

            return result;
        }
        
        let cartData = carts[0];
        result.positions = new Array<GetCartlistViewModelItem>();
        result.cartId = cartData.id.toString();
       
        if(cartData.positions === undefined || cartData.positions.length === 0){
            
            return result;
        }
        cartData.positions.forEach((position) => {

            let positionModel = new GetCartlistViewModelItem();
            positionModel.count = position.count;
            positionModel.itemId = position.itemId;

            result.positions.push(positionModel);
        });

        return result;
    }


    async AddPosition(positionModel: AddPositionModel, userId: string): Promise<ResultModel> {

        let result = new ResultModel();

        let cartData = await this.GetCartListByUserId(userId);
        let position = cartData.positions.find(position => position.itemId == positionModel.itemId);
        if(position === undefined){

            position = new CartPosition();
            position.itemId = positionModel.itemId;
            position.count = positionModel.count;
            cartData.positions.push(position);
            await this.cartRepository.update(cartData.cartId, {positions: cartData.positions} );

            result.isSuceed = true;
            result.message = "cart list";
            return result;
        }

        let index = cartData.positions.indexOf(position);
        position.count = positionModel.count;
        cartData.positions[index] = position;

        await this.cartRepository.update(cartData.cartId, {positions: cartData.positions} );

        result.isSuceed = true;
        result.message = "cart list";
        return result;
    }

    async UpdatePosition(positionModel: UpdatePositionModel, userId: string): Promise<ResultModel> {

        let result = new ResultModel();
        let cartData = await this.GetCartListByUserId(userId);
        let position = cartData.positions.find(position => position.itemId == positionModel.itemId);
        if(position === undefined){

            result.isSuceed = false;
            result.message = "position not found";
            return result;
        }

        if(positionModel.action === CartPositionAction.None){
            result.isSuceed = false; 
            result.message = "invalid action type";
            return result;
        }

        let index = cartData.positions.indexOf(position);
        position.count = (positionModel.action === CartPositionAction.Increment) 
        ? (position.count + 1) : (position.count - 1);
        cartData.positions[index] = position;

        await this.cartRepository.update(cartData.cartId, {positions: cartData.positions} );

        result.isSuceed = true; 
        result.message = "position updated";
        return result;        
    }


    async RemoveBookFromCart(removeBookModel: RemoveBookFromCartModel, userId: string): Promise<ResultModel> {

        let result = new ResultModel();
        let cartData = await this.GetCartListByUserId(userId);

        cartData.positions = cartData.positions.filter(position => position.itemId !== removeBookModel.bookId);

        await this.cartRepository.update(cartData.cartId, {positions: cartData.positions} );

        result.isSuceed = true; 
        result.message = "position updated";
        return result;
    }
}
