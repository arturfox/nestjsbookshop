import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';

@Entity()
export class Cart {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    userId: string;

    @Column()
    positions: CartPosition[];
}

export class CartPosition {
    
    itemId: string;
    count: number;
}