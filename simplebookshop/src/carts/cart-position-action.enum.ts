export enum CartPositionAction {
    None = 0,
    Increment = 1,
    Decrement = 2
} 