import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { SharedModule } from '../shared/shared.module';

@Module({
  imports: [SharedModule,
    TypeOrmModule.forFeature([UserRepository])],
  exports: [UsersService, TypeOrmModule],
  providers: [UsersService]
})
export class UsersModule { }
