import { Entity, Column, ObjectIdColumn, ObjectID } from 'typeorm';
import { UserRole } from './user-role.enum';

@Entity()
export class User {
    
    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    userName: string;

    @Column()
    email: string;

    @Column()
    role: UserRole; 

    @Column()
    hashPassword: string;
}