import { Repository, EntityRepository } from "typeorm";
import { User } from "./user.entity";

@EntityRepository(User)
export class UserRepository extends Repository<User>{

    async getByUserNameOrEmail(userName: string, email: string): Promise<User>{

        return await this.findOne({
            where:{
                $or:[
                    {
                        userName: userName 
                    },
                    {
                        email: email
                    }
                ]
            }
        });
    }

    async getByCredential(credential: string): Promise<User>{

        return await this.findOne({
            where:{
                $or:[
                    {
                        userName: credential 
                    },
                    {
                        email: credential
                    }
                ]
            }
        });
    }
}