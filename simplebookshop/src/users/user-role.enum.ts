export enum UserRole {
    None = 0,
    Viewer = 1,
    Admin = 2
}