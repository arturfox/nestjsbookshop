import { Module } from '@nestjs/common';
import { ShopService } from './shop.service';
import { ShopController } from './shop.controller';
import { SharedModule } from '../shared/shared.module';
import { CartsModule } from '../carts/carts.module';
import { BooksModule } from '../books/books.module';

@Module({
  imports: [SharedModule, CartsModule, BooksModule],
  providers: [ShopService],
  controllers: [ShopController]
})
export class ShopModule { }
