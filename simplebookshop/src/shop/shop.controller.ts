import { Controller, Get, Post, Body, Request, UseGuards } from '@nestjs/common';
import { ShopService } from './shop.service';
import { ResultModel } from '../shared/result-model';
import { GetBooksListViewModel } from '../books/view-models/get-books-list.viewmodel';
import { AddBookModel } from '../books/view-models/models/add-book.model';
import { GetUserCartDataViewModel } from './view-models/get-user-cart-data-view-model';
import { AuthGuard } from '@nestjs/passport';
import { UpdatePositionModel } from '../carts/view-models/models/update-position.model';
import { EditBookModel } from '../books/view-models/models/edit-book-model';
import { DeleteBookModel } from '../books/view-models/models/delete-book.model';
import { AddPositionModel } from '../carts/view-models/models/add-position.model';
import { RemoveBookFromCartModel } from '../carts/view-models/models/remove-book-from-cart.model';

@Controller('shop')
export class ShopController {

    constructor(private readonly shopService: ShopService) { }

    @UseGuards(AuthGuard('jwt'))
    @Get("catalog")
    async GetCatalog(@Request() request): Promise<ResultModel<GetBooksListViewModel>> {

        let userId = (request.user === undefined) ? null : request.user.userId;
        let result = await this.shopService.GetBooksCatalog(userId);
        return result;
    }

    @UseGuards(AuthGuard('jwt'))
    @Get("cart")
    async GetUserCartData(@Request() request): Promise<ResultModel<GetUserCartDataViewModel>> {

        let result = await this.shopService.GetCartListByUserId(request.user.userId);
        return result;

    }

    @UseGuards(AuthGuard('jwt'))
    @Post("cart/addPosition")
    async AddPosition(@Body() addPosition: AddPositionModel, @Request() request): Promise<ResultModel> {

        let result = await this.shopService.AddPosition(addPosition, request.user.userId);
        return result;
    }

    @UseGuards(AuthGuard('jwt'))
    @Post("cart/removeBook")
    async RemovePosition(@Body() removeBook: RemoveBookFromCartModel, @Request() request): Promise<ResultModel> {

        let result = await this.shopService.RemoveBookFromCart(removeBook, request.user.userId);
        return result;
    }


    @UseGuards(AuthGuard('jwt'))
    @Post("cart/updatePosition")
    async UpdatePosition(@Body() updatePosition: UpdatePositionModel, @Request() request): Promise<ResultModel> {

        let result = await this.shopService.UpdatePosition(updatePosition, request.user.userId);
        return result;
    }

    @UseGuards(AuthGuard('jwt'))
    @Post("admin/dashboard/add")
    async AddDashBoardItem(@Body() addBookModel: AddBookModel): Promise<ResultModel> {

        let result = await this.shopService.AddBook(addBookModel);
        return result;
    }

    @UseGuards(AuthGuard('jwt'))
    @Post("dashboard/delete")
    async DeleteDashBoardItem(@Body() deleteModel: DeleteBookModel, @Request() request): Promise<void> {

        let result = await this.shopService.DeleteBook(deleteModel.itemId, request.user.userId);
        return result;
    }

    @UseGuards(AuthGuard('jwt'))
    @Post("dashboard/edit")
    async EditDashBoardItem(@Body() editBookModel: EditBookModel, @Request() request): Promise<void> {

        let result = await this.shopService.EditBook(editBookModel, request.user.userId);
        return result;
    }
}