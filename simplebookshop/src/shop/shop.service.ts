import { Injectable } from '@nestjs/common';
import { BooksService } from '../books/books.service';
import { GetBooksListViewModel, GetBooksListViewModelItem } from '../books/view-models/get-books-list.viewmodel';
import { ResultModel } from '../shared/result-model';
import { AddBookModel } from '../books/view-models/models/add-book.model';
import { CartsService } from '../carts/carts.service';
import { GetUserCartDataViewModel, GetUserCartDataViewModelItem } from './view-models/get-user-cart-data-view-model';
import { AddPositionModel } from '../carts/view-models/models/add-position.model';
import { ObjectID } from 'mongodb';
import { UpdatePositionModel } from '../carts/view-models/models/update-position.model';
import { EditBookModel } from '../books/view-models/models/edit-book-model';
import { GetBooksCatalogShopViewModel, GetBooksCatalogShopViewModelItem } from './view-models/get-books-catalog-shop.view-model';
import { RemoveBookFromCartModel } from '../carts/view-models/models/remove-book-from-cart.model';

@Injectable()
export class ShopService {

    constructor(private readonly booksService: BooksService,
        private readonly cartsService: CartsService) {

    }

    async GetBooksCatalog(userId: string): Promise<ResultModel<GetBooksCatalogShopViewModel>> {

        let catalog = new ResultModel<GetBooksCatalogShopViewModel>();

        const books = await this.booksService.GetBooks();
        const shopBooks = await this.GetExpandedBooksCatalogByUserId(books, userId);

        catalog.data = shopBooks;
        catalog.isSuceed = true;
        catalog.message = "books catalog";

        return catalog;
    }


    async GetExpandedBooksCatalogByUserId(books: GetBooksListViewModel, userId: string): Promise<GetBooksCatalogShopViewModel> {

        let catalog = new GetBooksCatalogShopViewModel();
        catalog.Books = new Array<GetBooksCatalogShopViewModelItem>();

        if (userId === null) {
            books.Books.forEach(element => {
                let bookView = this.MapGetBooksListViewModelItemToGetBooksCatalogShopViewModelItem(element);
                bookView.isAddedtoCart = false;

                catalog.Books.push(bookView);
            });
            return catalog;
        }

        const cartList = await this.cartsService.GetCartListByUserId(userId);
        books.Books.forEach(element => {
            let bookView = this.MapGetBooksListViewModelItemToGetBooksCatalogShopViewModelItem(element);
            let isBookAddedToCart = cartList.positions.some(position => position.itemId === element.id);
            bookView.isAddedtoCart =  isBookAddedToCart;

            catalog.Books.push(bookView);
        });

        return catalog;
    }

    MapGetBooksListViewModelItemToGetBooksCatalogShopViewModelItem(item: GetBooksListViewModelItem): GetBooksCatalogShopViewModelItem {

        let bookView = new GetBooksCatalogShopViewModelItem();
        bookView.author = item.author;
        bookView.count = item.count;
        bookView.description = item.description;
        bookView.id = item.id;
        bookView.pages = item.pages;
        bookView.price = item.price;
        bookView.published = item.published;
        bookView.publisher = item.publisher;
        bookView.source = item.source;
        bookView.title = item.title;

        return bookView;
    }



    AddBook(bookModel: AddBookModel): Promise<ResultModel> {
        return this.booksService.AddBook(bookModel);
    }

    async DeleteBook(bookId: string, userId: string): Promise<void> {
        const isBookExist = await this.booksService.CheckIsBookExist(bookId);
        if (isBookExist) {
            await this.booksService.DeleteBook(bookId);
        }
    }

    async EditBook(editBookModel: EditBookModel, userId: string): Promise<void> {
        const isBookExist = await this.booksService.CheckIsBookExist(editBookModel.itemId);
        if (isBookExist) {
            await this.booksService.EditBook(editBookModel);
        }
    }

    async GetCartListByUserId(userId: string): Promise<ResultModel<GetUserCartDataViewModel>> {
        let cartModel = new ResultModel<GetUserCartDataViewModel>();
        cartModel.data = new GetUserCartDataViewModel();
        cartModel.data.positions = new Array<GetUserCartDataViewModelItem>();

        let cartData = await this.cartsService.GetCartListByUserId(userId);
        let booksInfo = await this.booksService.GetBooksShortDataByIds(cartData.positions.map(p => new ObjectID(p.itemId)));

        cartData.positions.forEach((p) => {

            let cart = new GetUserCartDataViewModelItem();
            cart.bookId = p.itemId;
            cart.count = p.count;

            let book = booksInfo.find(x => x.bookId == p.itemId);
            if (book == null) {
                return;
            }

            cart.source = book.source;
            cart.title = book.title;
            cart.totalPrice = book.price * p.count;

            cartModel.data.positions.push(cart);
        });

        cartModel.isSuceed = true;

        return cartModel;
    }

    AddPosition(positionModel: AddPositionModel, userId: string): Promise<ResultModel> {
        return this.cartsService.AddPosition(positionModel, userId);
    }

    UpdatePosition(positionModel: UpdatePositionModel, userId: string): Promise<ResultModel> {
        return this.cartsService.UpdatePosition(positionModel, userId);
    }

    RemoveBookFromCart(removeBookModel: RemoveBookFromCartModel, userId: string): Promise<ResultModel> {
        return this.cartsService.RemoveBookFromCart(removeBookModel, userId);
    }
}