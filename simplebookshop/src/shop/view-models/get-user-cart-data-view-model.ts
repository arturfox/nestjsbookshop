export class GetUserCartDataViewModel {
    
    positions: GetUserCartDataViewModelItem[];
}

export class GetUserCartDataViewModelItem {

    bookId: string;

    count: number;

    title: string;  

    source: string;

    totalPrice: number;
}