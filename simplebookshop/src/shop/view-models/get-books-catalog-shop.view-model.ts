export class GetBooksCatalogShopViewModel {

    Books: GetBooksCatalogShopViewModelItem[];

    constructor() {
    }
}

export class GetBooksCatalogShopViewModelItem {

    id: string;

    title: string;

    author: string;

    publisher: string;

    description: string;

    source: string;

    count: Number;

    pages: Number;

    price: Number;

    published: Date;

    isAddedtoCart: boolean;
}