export class ResultModel<T = void>{
    
    public data: T;
    public message: string;
    public isSuceed: boolean;
}