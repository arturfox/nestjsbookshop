export class ResponseLoginModel {

    public accessToken: string;
    public refreshToken: string;
    public userId: string;
}