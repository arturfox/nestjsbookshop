import { Entity, Column, ObjectID, ObjectIdColumn } from 'typeorm';

@Entity()
export class RefreshToken {
  
  @ObjectIdColumn()
  id: ObjectID;

  @Column()
  token: string;

  @Column()
  acessTokenPrint: string;

  @Column()
  isValid: boolean;

  @Column()
  userId: string;

  @Column()
  creationDate: string;

  @Column()
  lifeTime: string;

}