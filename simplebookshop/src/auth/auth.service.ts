import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { ResultModel } from '../shared/result-model';
import { User } from '../users/user.entity';
import { ResponseLoginModel } from './response-login.model';
import * as crypto from 'crypto';
import { LoginModel } from './view-models/models/login.model';
import { RegisterModel } from './view-models/models/register.model';
import { jwtConstants, refreshConstants } from './constants';
import * as UUID from 'uuid';
import { RefreshTokenRepository } from './refresh-token.repository';
import { RefreshToken } from './refresh-token.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { RefreshTokenModel } from './view-models/models/refresh-token.model';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
    @InjectRepository(RefreshToken)
    private readonly refreshTokenRepository: RefreshTokenRepository) { }

  async validateUser(credential: string, password: string): Promise<any> {
    const user = await this.usersService.getByCredential(credential);
    if (user && user.hashPassword === crypto.createHmac('sha256', password).digest('hex')) {
      const { hashPassword, ...result } = user;
      return result;
    }
    return null;
  }

  //TODO: add AcessToken blacklist
  public async login(loginModel: LoginModel): Promise<ResultModel<ResponseLoginModel>> {

    let result = new ResultModel<ResponseLoginModel>();

    result = await this.loginDefault(loginModel);
    return result;
  }

  public async GetUpdatedTokensByRefreshToken(refreshModel: RefreshTokenModel): Promise<ResultModel<ResponseLoginModel>> {

    let result = new ResultModel<ResponseLoginModel>();

    let token = await this.refreshTokenRepository.getByTokenBody(refreshModel.refreshToken);

    if (token == null || !token.isValid) {

      result.isSuceed = false;
      result.message = "refresh token is invalid";

      return result;
    }

    let criationdate = +token.creationDate;
    let lifetime = +token.lifeTime * refreshConstants.secondsToMillisecondsCoefficient;

    let tokenDate = new Date(criationdate + lifetime);

    if (tokenDate.valueOf() < Date.now()) {

      result.isSuceed = false;
      result.message = "refresh token is invalid";

      return result;
    }

    const existingUser = await this.usersService.getByUserId(token.userId);
    if (existingUser == null) {

      result.isSuceed = false;
      result.message = "User is not found";

      return result;
    }

    await this.refreshTokenRepository.update(token.id.toString(), { isValid: false });

    result.isSuceed = true;
    result.message = "User successfully authorized";
    result.data = await this.GetResponseLoginModelByUser(existingUser, this.jwtService);

    return result;

  }

  public async register(registerModel: RegisterModel): Promise<ResultModel> {

    let result = new ResultModel();

    if (registerModel.password !== registerModel.confirmPassword) {
      result.isSuceed = false;
      result.message = "password and re-password do not match";

      return result;
    }

    const existingUser = await this.usersService.getByUserNameOrEmail(registerModel.username, registerModel.email);

    if (existingUser) {
      result.isSuceed = false;
      result.message = "a user with the same name/email already exists";

      return result;
    }

    let user = new User();
    user.userName = registerModel.username;
    user.email = registerModel.email;
    user.role = registerModel.role;
    user.hashPassword = crypto.createHmac('sha256', registerModel.password).digest('hex');

    const createResult = await this.usersService.AddUser(user);
    result.isSuceed = createResult.isSuceed;

    result.message = (result.isSuceed) ? "account successfully created"
      : "something went wrong while creating account";

    return result;
  }

  private async loginDefault(loginModel: LoginModel): Promise<ResultModel<ResponseLoginModel>> {

    let result = new ResultModel<ResponseLoginModel>();
    const existingUser = await this.usersService.getByCredential(loginModel.credential);

    if (existingUser == null || existingUser == undefined) {

      result.isSuceed = false;
      result.message = "User is not found";

      return result;
    }

    const hashPassword = crypto.createHmac('sha256', loginModel.password).digest('hex');
    if (hashPassword != existingUser.hashPassword) {

      result.isSuceed = false;
      result.message = "Username or password is incorrect";

      return result;
    }

    result.isSuceed = true;
    result.message = "User successfully authorized";
    result.data = await this.GetResponseLoginModelByUser(existingUser, this.jwtService);

    return result;
  }

  private async  GetResponseLoginModelByUser(user: User, jwtService: JwtService): Promise<ResponseLoginModel> {

    let responseModel = new ResponseLoginModel();

    responseModel.userId = user.id.toString();
    responseModel.accessToken = await jwtService.signAsync({
      username: user.userName,
      email: user.email,
      userrole: user.role,
      userId: user.id
    },
      {
        expiresIn: jwtConstants.expiresIn
      }
    );
    responseModel.refreshToken = await this.generateRefreshToken(responseModel.userId, responseModel.accessToken);

    return responseModel;
  }

  private async generateRefreshToken(userId: string, accessToken: string): Promise<string> {

    let token = new RefreshToken();
    token.acessTokenPrint = accessToken;
    token.isValid = true;
    token.lifeTime = refreshConstants.expiresIn;
    token.creationDate = Date.now().toString();
    token.userId = userId;
    token.token = UUID.v4();

    await this.refreshTokenRepository.insert(token);
    return token.token;
  }
}
