import { Repository, EntityRepository } from "typeorm";
import { RefreshToken } from "./refresh-token.entity";

@EntityRepository(RefreshToken)
export class RefreshTokenRepository extends Repository<RefreshToken>{

    async getByTokenBody(token: string): Promise<RefreshToken>{

        return await this.findOne({
            where:{
                token: token
            }
        });
    }
}