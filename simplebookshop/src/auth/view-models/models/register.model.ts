import { MinLength, IsString, IsEnum } from 'class-validator';
import { UserRole } from '../../../users/user-role.enum';

export class RegisterModel {

    @IsString()
    public username: string;

    @IsString()
    public email: string;

    @IsEnum(UserRole)
    public role: UserRole;

    @IsString()
    @MinLength(6)
    public password: string;

    @IsString()
    @MinLength(6)
    public confirmPassword: string;
}