import { IsString } from 'class-validator';

export class LoginModel {

    @IsString()
    public credential: string;

    public password: string;
}