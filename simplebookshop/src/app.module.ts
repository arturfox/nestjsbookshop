import { Module } from '@nestjs/common';
import { BooksModule } from './books/books.module';
import { CartsModule } from './carts/carts.module';
import { ShopModule } from './shop/shop.module';
import { AuthModule } from './auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from './shared/shared.module';
import { Book } from './books/book.entity';
import { User } from './users/user.entity';
import { Cart } from './carts/cart.entity';
import { RefreshToken } from './auth/refresh-token.entity';

@Module({
  imports: [
    SharedModule,
    BooksModule,
    CartsModule,
    ShopModule,
    AuthModule,
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host: 'localhost',
      port: 27017,
      database: 'simplebookshopdb',
      entities: [Book, User, Cart, RefreshToken],
      synchronize: true
    })],
  exports: [SharedModule]
})
export class AppModule { }